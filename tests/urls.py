# -*- coding: utf-8
from __future__ import unicode_literals, absolute_import

from django.conf.urls import url, include

from workflow.urls import urlpatterns as workflow_urls

urlpatterns = [
    url(r'^', include(workflow_urls, namespace='workflow')),
]
