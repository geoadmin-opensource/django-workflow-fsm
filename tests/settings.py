# -*- coding: utf-8
from __future__ import unicode_literals, absolute_import
import os
import logging
import django
from celery import Celery


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
DEBUG = True
USE_TZ = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "dnf*2n0-8egtb3s55sz8v(_12ho-b#*h!%!^x&2g(i+da6@7kb"

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": "tests.db",
    }
}

ROOT_URLCONF = "tests.urls"
LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'workflow', 'locale'),
)
INSTALLED_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sites",
    'common',
    "workflow",
]

SITE_ID = 1

if django.VERSION >= (1, 10):
    MIDDLEWARE = ()
else:
    MIDDLEWARE_CLASSES = ()

WORKFLOW_AUTO_LOAD = True
CELERY_TASK_ALWAYS_EAGER = True
CELERY_BROKER_URL = 'redis://localhost:6379/0'
CELERY_RESULT_BACKEND = CELERY_BROKER_URL

app = Celery('workflow-fsm')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks(lambda: INSTALLED_APPS)

logging.disable(logging.CRITICAL)
