============
Installation
============

At the command line::

    $ easy_install django-workflow-fsm

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv django-workflow-fsm
    $ pip install django-workflow-fsm
