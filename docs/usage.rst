=====
Usage
=====

To use Django Workflow FSM in a project, add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'workflow.apps.WorkflowConfig',
        ...
    )

Add Django Workflow FSM's URL patterns:

.. code-block:: python

    from workflow import urls as workflow_urls


    urlpatterns = [
        ...
        url(r'^', include(workflow_urls)),
        ...
    ]
